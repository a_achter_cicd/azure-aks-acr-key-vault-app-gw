resource "kubernetes_namespace" "example" {
  for_each = toset( ["app1a", "app2a"] )
  metadata {
    annotations = {
      name = "example-annotation"
    }

    labels = {
      mylabel = "label-value"
    }

    name = each.key
  }
  depends_on = [azurerm_kubernetes_cluster.aks]
}
