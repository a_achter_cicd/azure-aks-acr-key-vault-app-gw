# Generate a random rg name
resource "random_string" "rg-name" {
  length  = 12
  upper   = false
  number  = false
  lower   = true
  special = false
}

# Set local var RG Name
locals {
  rg_name = "${random_string.rg-name.result}-rg"
}

resource "azurerm_resource_group" "rg" {
  name     = local.rg_name
  location = var.location

  lifecycle {
    ignore_changes = [
      tags,
    ]
  }
}


