# Generate a random resource name
resource "random_string" "random-name" {
  length  = 12
  upper   = false
  number  = false
  lower   = true
  special = false
}

# Set random Vault and Resource Names
locals {
  vault_name    = "kv-${random_string.random-name.result}"
  rand_res_name = "${random_string.random-name.result}"
}

# Create a resource group for this deployment
module "resource_group" {
  source = "./modules/resource_group"

  location = var.location
  name     = "${var.app_name}-rg"
}

# Create a common key vault to store application secrets
module "keyvault" {
  source = "./modules/key_vault"

  name                = local.vault_name
  location            = var.resource_location
  resource_group_name = module.resource_group.name

  # Config
  enabled_for_deployment          = "true"
  enabled_for_disk_encryption     = "true"
  enabled_for_template_deployment = "true"
}

# Create the Azure Container Registry
module "acr" {
  source = "./modules/acr"

  name                = "acr${local.rand_res_name}"
  resource_group_name = module.resource_group.name
  location            = var.location
}

# Create virtual network
module "vnet" {
  source = "./modules/vnet"

  name                = "vnet-${var.app_name}"
  resource_group_name = module.resource_group.name
  location            = var.location
}

# Create Log Analytics Insights #
module "log_analytics" {
  source = "./modules/log_analytics"

  app_name            = var.app_name
  resource_group_name = module.resource_group.name
  location            = var.location
}

# Create AKS Cluster
module "aks" {
  source = "./modules/aks"

  resource_group_name  = module.resource_group.name
  app_name             = local.rand_res_name
  location             = var.location
  virtual_network_name = module.vnet.name

  acr_id           = module.acr.id
  key_vault_id     = module.keyvault.key_vault_id
  log_analytics_id = module.log_analytics.id

  ### AKS configuration params ###
  kubernetes_version  = var.kubernetes_version
  vm_size_node_pool   = var.vm_size_node_pool
  node_pool_min_count = var.node_pool_min_count
  node_pool_max_count = var.node_pool_max_count

  ### Helm Chart versions ###
  helm_pod_identity_version = var.helm_pod_identity_version
  helm_csi_secrets_version  = var.helm_csi_secrets_version
  helm_keda_version         = var.helm_keda_version
}


# Create Application Gateway
module "appgw" {
  source = "./modules/appgw"

  resource_group       = { "name" : module.resource_group.name, "id" : module.resource_group.id }
  app_name             = var.app_name
#  namesp_agic          = local.rand_res_name
  location             = var.location
  virtual_network_name = module.vnet.name
  aks_object_id        = module.aks.kubelet_identity
  aks_config           = module.aks.aks_config
  domain_name_label    = var.domain_name_label

  ### Helm Chart versions ###
#  helm_agic_version = var.helm_agic_version
}

